import UIKit



protocol Food{
    var title : String { get }
    func tasting()
        //print("\(title) - tasty")
    
}

protocol Storable {
    var expired : Bool {get set}
    var daysToExpired : Int {get set}
    var name : String {get set}
}

class Berry : Food, Storable{
    var name : String
    init(name : String, expired : Bool, daysToexpired : Int){
        self.name = name
        self.expired = expired
        self.daysToExpired = daysToexpired
    }
    var title : String{
        return name
    }
    func tasting(){
        print(title + "So tasty")
    }
    var expired : Bool
    var daysToExpired: Int
}

class Sausage : Food, Storable{
    var name : String
    var firm : String
    var count : Int
    init(name : String, firm : String, count : Int, expired : Bool, daysToExpired : Int){
        self.name = name
        self.firm = firm
        self.count = count
        self.daysToExpired = daysToExpired
        self.expired = expired
    }
    var title : String{
        return name
    }
    func tasting(){
        print(title + "nutritionally")
    }
    var expired : Bool
    var daysToExpired: Int
}

class Bread : Food{
    var name : String
    enum Types : String{
        case white = "white"
        case rye = "rye"
        case wheat = "wheat"
        case pita = "pita"
    }
    var type : Types
    init(name : String, type : Types){
        self.name = name
        self.type = type
    }
    var title : String{
        return ("\(name) \(type.rawValue)")
    }
    func tasting(){
        print(title + "nutritionally")
    }
}

class Meat : Food, Storable{
    var animal : String
    var name : String
    init(name : String, animal : String, expired : Bool, daysToExpired : Int){
        self.name = name
        self.animal = animal
        self.expired = expired
        self.daysToExpired = daysToExpired
    }
    var title : String{
        return name + animal
    }
    func tasting(){
        print(title + "nutritionally")
    }
    var expired : Bool
    var daysToExpired: Int
}

class Drinks : Food{
    var name : String
    enum Sugar : String{
        case yes = "more sugar"
        case no = "0% sugar"
    }
    var sugar : Sugar
    enum Soda : String{
        case yes = "more soda"
        case no = "no soda"
    }
    var soda : Soda
    init(name : String, sugar : Sugar, soda : Soda){
        self.name = name
        self.sugar = sugar
        self.soda = soda
    }
    var title : String{
        return name
    }
    func tasting(){
        print(title + " refreshingly")
    }
}
class Sweet : Food{
    var name : String
    enum Types : String{
        case chocolate = "chocolate"
        case candies = "candies"
        case cookies = "cookies"
        case snacks = "snacks"
    }
    var type : Types
    init(name : String, type : Types){
        self.name = name
        self.type = type
    }
    var title : String{
        return name
    }
    func tasting() {
        print(title + "tasty")
    }
}

class Fruit : Food{
    var name : String
    enum Types : String{
        case apple = "apple"
        case pear = "pear"
        case banana = "bananas"
        case orange = "orange"
    }
    var type : Types
    init(name : String, type : Types){
        self.type = type
        self.name = name
    }
    var title : String{
        return type.rawValue
    }
    func tasting(){
        print(title + "nutritionally")
    }
}

class Vegetables : Food{
    var name : String
    enum Types : String{
        case cucumber = "cucumber"
        case potato = "potato"
        case beet = "beet" //свекла
        case onion = "onion"
        case garlic = "garlic"
    }
    var type : Types
    init(name : String, type : Types){
        self.name = name
        self.type = type
    }
    var title : String{
        return type.rawValue
    }
    func tasting(){
        print(title + "nutritionally")
    }
}


var cola = Drinks(name: "Coca-cola", sugar: Drinks.Sugar.yes, soda: Drinks.Soda.yes)
var steak = Meat(name: "Steak", animal: "cow", expired: false, daysToExpired: 1)
var blueberry = Berry(name: "blueberry", expired: true, daysToexpired: 0)
var cervelat = Sausage(name: "Doctor", firm: "Firm", count: 2, expired: false, daysToExpired: 2)





var package : [Food] = [cola, blueberry, cervelat, steak]

func sortPackage(arrayIn : [Food]) -> [Food]{
    var array = arrayIn
    array.sort {$0.title > $1.title}
    return array
}
func printPackage(array : [Food]){
    for i in array{
        print(i.title)
    }
}

func sortFridge(arrayIn : [Storable]) -> [Storable]{
    var array = arrayIn
    array.sort {$0.daysToExpired < $1.daysToExpired}
    return array
}

var fridge : [Storable] = []

func checkPackage(array : [Food]) -> [Storable]{
    var arrayOut : [Storable] = []
    for i in array{
        if let j = i as? Storable{
            if j.expired == false{
                arrayOut.append(j)
            }
        }
    }
    return arrayOut
}

//package = sortPackage(arrayIn: package)

fridge = checkPackage(array: package)


fridge = sortFridge(arrayIn: fridge)


func printFrige(array : [Storable]){
    for i in array{
        print(i.name)
    }
}

printFrige(array: fridge)


cola.tasting()








/////////////////////////////////////////////////////////
protocol ConteinerLiFo : Collection{
    mutating func pop() -> Self.Iterator.Element
    mutating func push(element : Self.Iterator.Element)
}
protocol ConteinerFiFo : Collection{
    mutating func dequeue() -> Self.Iterator.Element
    mutating func enqueue(element : Self.Iterator.Element)
}
extension Array : ConteinerLiFo, ConteinerFiFo{
    mutating func pop() -> Self.Iterator.Element {
        let temp = self.popLast()
        return temp!
    }
    mutating func push(element: Self.Iterator.Element) {
        self.append(element)
    }
    mutating func dequeue() -> Self.Iterator.Element {
        let temp = self.popLast()
        return temp!
    }
    mutating func enqueue(element: Self.Iterator.Element){
        self.insert(element, at: 0)
    }

}
var t = [1,2,3,4]
t.pop()

t.push(element: 5)


t.insert(1, at: 0)

t.enqueue(element: 0)

t.dequeue()
t




//Пытался разобраться с extension Conteiner, получилось только то, что вверху

//
//
//protocol Conteiner : Collection {
//    mutating func pop() -> Self.Iterator.Element
//    mutating func push(element : Self.Iterator.Element)
//}
//extension Conteiner {
//    mutating func dequeue() -> Self.Iterator.Element{
//        let temp = self.popLast
//        return temp
//    }
//    mutating func enqueue(element : Self.Iterator.Element){
//        self.insert(element, at: 0)
//    }
//
//}
////protocol ConteinerFiFo : Collection{
////    mutating func dequeue() -> Self.Iterator.Element
////    mutating func enqueue(element : Self.Iterator.Element)
////}
//extension Array : Conteiner/*LiFo, ConteinerFiFo*/{
//    mutating func pop() -> Self.Iterator.Element {
//        let temp = self.popLast()
//        return temp!
//    }
//    mutating func push(element: Self.Iterator.Element) {
//        self.append(element)
//    }
//
////    mutating func dequeue() -> Self.Iterator.Element {
////        let temp = self.popLast()
////        return temp!
////    }
////    mutating func enqueue(element: Self.Iterator.Element){
////        self.insert(element, at: 0)
////    }
//
//}
//var t = [1,2,3,4]
//t.pop()
//
//t.push(element: 5)
//
//
//t.insert(1, at: 0)
//
////t.enqueue(element: 0)
////
////t.dequeue()
////t
//t.dequeue()
//t
